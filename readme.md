## Data processing
In this repository, all snakemake pipelines will be delivered.
Eventually, a snakemake pipeline will be made as a final
assignment.

### Week 1
- In this week, there was made a snakemake file which fetches
  input data from the server, and the filenames were
  concatenated using python. Also, there was added a third input.
  input: empty zip files
  output: test.txt

### Week 2
- Excercise 1: looked for 4 complete genomes on NCBI and
  created a multifasta file for all sequences in the files.
  input: query fasta files
  output: genomes.txt (multifasta file)

- Excercise 2: data from week 1 has been moved to another
  location in the commons directory
  input: empty zip file
  output: test.txt in data folder

- Excercise 3: created a config file


### Final Assignment:
- This final assignment is a pipeline to perform spliceQTL analysis on an asthma dataset, using split-read data. The amount
  of samples, or persons in this dataset is 232. However, not every person has every level of data, e.g.
  one person can have expression, or split-read data, but not have imputed genotype data. The amount of overlapping
  persons is 147, so they have all levels of data. The analysis is run using matrixEQTL, which fits a linear regresseion between
  the genotype data and the expression data. The tophit snp is visualized by plotting the genotype versus the expression
